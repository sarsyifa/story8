$(document).ready(function(){ 
    findBook("quilting")
});

function findBook(judul) {
    $.ajax({
        url: "get_data/cari="+judul,
        datatype: 'json',
        success: function(data){
            $('tbody').empty();
            var result;
            for(var i = 0; i < data.items.length; i++) {
                var image;
                if (!("imageLinks" in data.items[i].volumeInfo)) {
                    image = "https://www.iconspng.com/images/icon-book/icon-book.jpg";
                } else {
                    image = data.items[i].volumeInfo.imageLinks.smallThumbnail; 
                }
                result += '<tr>'
                result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + image +"'></img>" + "</td>" +
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                "</tr>";
            }
            $('tbody').append(result);
        }
    })
}




var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		document.getElementById("counter").innerHTML = counter;
	}
}

    
function search() {
    var word = $('input[name="pencarian"]').val();
    findBook(word);
}
