from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth import logout as logout_oauth
import requests
import json

# Create your views here.

first_log = True

def get_data(request, cari="quilting"):

    link = "https://www.googleapis.com/books/v1/volumes?q=" + cari
    data_final = requests.get(link).json()
    return JsonResponse(data_final)

def index(request):
    if  request.user.is_authenticated :
        if "liked_books" not in request.session:
            request.session["fullname"] = request.user.first_name + " " + request.user.last_name
            request.session["username"] = request.user.username
            request.session["email"] = request.user.email
            request.session["sessionid"] = request.session.session_key
            request.session['liked_books'] = []

        return render(request, 'list_book_user.html')
    else:
        request.session['liked_books'] = []
        return render(request, 'list_book_no_user.html') 

def get_book(request):
    return JsonResponse(request.session['liked_books'], safe = False)

def addFav(request):
    if request.method == "POST":
        liked_book_id = request.POST['liked_id']
        tmp_liked_books = request.session['liked_books']
        if liked_book_id not in tmp_liked_books:
            tmp_liked_books.append(liked_book_id)
            request.session['liked_books'] = tmp_liked_books

        return JsonResponse({'result' : 'success', 'status' : 200})

    return JsonResponse({"result" : "No Get Method", 'status' : 403})


def unFav(request):
    if request.method == "POST":
        liked_book_id = request.POST['liked_id']
        tmp_liked_books = request.session['liked_books']

        if liked_book_id in tmp_liked_books:
            tmp_liked_books.remove(liked_book_id)
            request.session['liked_books'] = tmp_liked_books

        return JsonResponse({'result' : 'success', 'status' : 200})
    
    return JsonResponse({"result" : "No Get Method", 'status' : 403})

def logout(request):
    logout_oauth(request)
    return redirect('story11:index')

