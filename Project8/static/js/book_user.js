$(document).ready(function(){ 
    get_book("quilting");
});

function get_book(keyword) {
    $.ajax({
        url: "get_book/",
        success: function(data) {
            findBook(keyword, data);
        }
    })
}

function findBook(judul, book_list) {
    $.ajax({
        url: "get_data/cari="+judul,
        datatype: 'json',
        success: function(data){
            $('tbody').empty();
            document.getElementById("counter").innerHTML = book_list.length;
            var result, liked, image_src;
            for(var i = 0; i < data.items.length; i++) {
                liked = ($.inArray(data.items[i].id, book_list) > -1) ? 'class:"checked"' : "";
                image_src = (liked == "") ? "https://image.flaticon.com/icons/svg/149/149222.svg" : "https://image.flaticon.com/icons/svg/291/291205.svg";
                console.log(liked)
                console.log($.inArray(data.items[i].id, book_list))
                
                if (!("imageLinks" in data.items[i].volumeInfo)) {
                    image = "https://www.iconspng.com/images/icon-book/icon-book.jpg";
                } else {
                    image = data.items[i].volumeInfo.imageLinks.smallThumbnail; 
                }
                result += '<tr>'
                result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + image +"'></img>" + "</td>" +
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td style='padding: 15px;' class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + 
                "<img id='" + data.items[i].id + 
                "' onclick='favorite(this.id)' " + liked + " width='28px' src='" + image_src + "'>" + 
                "</td></tr>";
            }
            $('tbody').append(result);
        }
    })
}




var counter = 0;
function favorite(clicked_id){
    var btn = document.getElementById(clicked_id);
    console.log(btn);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
        document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
        unLike(clicked_id);
		counter--;
		document.getElementById("counter").innerHTML = counter;
	} else {
		btn.classList.add('checked');
        document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
        addFav(clicked_id);
		counter++;
		document.getElementById("counter").innerHTML = counter;
	}
}

function addFav(clicked_id) {

    data = {
      "liked_id" : clicked_id,
      "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
      type:"POST",
      url: "addFav/",
      data: data
    });
}
  

function addFav(clicked_id) {
    data = {
        "liked_id" : clicked_id,
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
      }
      $.ajax({
        type:"POST",
        url: "unFav/",
        data: data
      });
}

  
function search() {
    var word = $('input[name="pencarian"]').val();
    get_book(word);
}
