$(document).ready(function() {
	var acc = $(".accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
		 	var panel = this.nextElementSibling;
		    	if (panel.style.maxHeight){
		      		panel.style.maxHeight = null;
		    	} else {
		      		panel.style.maxHeight = panel.scrollHeight + "px";
		    	} 
		  	});
		}

		var color = true;
		$("#demo").click(function(){
			if (color) {
				$("body").css("background-color", "black");
				color = false;
			}
			else{
				$("body").css("background-color", "#ffffff");
				color = true;
			}
		});

$.fn.center = function () {
		this.css("position","absolute");
	  	this.css("top", Math.max(0, (
	    ($(window).height() - $(this).outerHeight()) / 2) + 
	    $(window).scrollTop()) + "px"
	  	);
	  	this.css("left", Math.max(0, (
	    ($(window).width() - $(this).outerWidth()) / 2) + 
	    $(window).scrollLeft()) + "px"
	  	);
	  	return this;
	}

	$("#overlay").show();
	$("#overlay-content").show().center();

	setTimeout(function(){    
	  $("#overlay").fadeOut();
	}, 2000);


	$(function(){
    $.ajax({
      type : "GET",
      url: '/data',
      dataType: 'json',
      success: function(data){
        var result = ''
        var temp = data['items']
        for (var i = 0; i<temp.length; i++) {
          item = temp[i]
          var img = item['volumeInfo']['imageLinks']['smallThumbnail']
          var title = item['volumeInfo']['title']
          var author = item['volumeInfo']['authors']
          var desc = item['volumeInfo']['description']

          result += "<tr><td><img src =" + img + "></td>"
          result += "<td>" + title + "</td>"
          result += "<td>" + author + "</td>"
          result += "<td>" + desc + "</td>"
          result += "<td><button type='button' class='tombol btn btn-dark'>Favorite</button></td></tr>"
        }
        $("#bt").html(result)
      }
    });
  });
  var fav = 0;
  var unfav_bt = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/A_Black_Star.png/689px-A_Black_Star.png"
  var fav_bt = "https://upload.wikimedia.org/wikipedia/commons/2/29/Gold_Star.svg"
  $(document).on('click', '.tombol', function(){
    if ($(this).hasClass('clicked')) {
      fav--;
      $(this).removeClass('clicked');
      $(this).attr('src', unfav_bt)
      //$(this).css("background-color","#343a40");
      //$(this).css("color", "white");
    }
    else{
      fav++;
      $(this).addClass('clicked');
      $(this).attr('src', fav_bt)
      //$(this).css("background-color", "yellow");
      //$(this).css("color", "black");
    }
    $("#jml").html(fav)
  });
});

$(document).ready(function(){
  $("#sbar").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#bt tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});



