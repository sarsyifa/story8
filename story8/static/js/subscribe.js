var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkEmail();
        checkAll();
    });
    
    $("#emailForm").keyup(function() {
        checkEmail();
    });
    
    $("#passwordForm").keyup(function() {
        $('#statusForm').html('');
        if ($('#passwordForm').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }else{
            checkEmail()
        }
        checkAll();
    });

   // $('input').focusout(function() {
   //     checkEmail()
    //});

    $('#submit').click(function () {
        data = {
            'email' : $('#emailForm').val(),
            'name' : $('#nameForm').val(),
            'password' : $('#passwordForm').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'add_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('emailForm').value = '';
                document.getElementById('nameForm').value = '';
                document.getElementById('passwordForm').value = '';
                
                $('#statusForm').html('');
                checkAll();
                //
                showSubs();
            }
        })
    });

    showSubs();
})



function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#nameForm').val() !== '' && 
        $('#passwordForm').val() !== '' &&
        $('#passwordForm').val().length > 7) {
        
        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}

function showSubs() {
    $('tbody').empty();
    $.ajax({
        type: 'GET',
        url:'web_service',
        success: function(data) {
            var i = 0
            for(i; i<data.length; i++) {
                content = '<tr class="align-middle">' + 
                "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" + 
                '<td>' + data[i].name + '</td><td>' + data[i].email + 
                '</td><td class="text-center"> <button class="button" value="' + 
                data[i].email + '" onclick="deleteSubs(this.value)"> Unsubscribe </button> </td></tr>';
                $('tbody').append(content)
            }
        }
    });
}

function deleteSubs(email) {
    $(".modal").css('display', 'block');
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
        $(".modal").css('display', 'none');
        $('#modal-content').empty();
        $('#modal-content').append("<h3 id='email_delete'>Enter password:</h3><input type='password' id='password-delete' name='password-delete'><button id='delete_button' class='button'> Delete </button><div id='status-deleted'></div>")
    }

    $('#delete_button').click(function() {
        data = {
            'password':$('#password-delete').val(),
            'email':email,
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }

        $.ajax({
            type: 'POST',
            url:'delete_user/',
            data: data,
            dataType: 'json',
            success: function(data) {
                console.log('login');
                if (data['deleted']) {
                    showSubs();
                    $('#modal-content').empty()
                    $('#modal-content').append('<div class="text-center"><h2>'+data['message']+'</h2><div>')
                } else {
                    $('#status-deleted').empty();
                    $('#status-deleted').append(data['message'])   
                }
               
            }
        });
    });
}
