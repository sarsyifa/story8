from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login
import requests
import json


from .forms import SubscribeForm
from .models import Subscriber
from django.core.validators import validate_email
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

response = {}

def index(request):
	response = {
		'subsForm' : SubscribeForm
	}
	return render(request, 'index.html', response)

def books(request):
    # if request.user.is_authenticated:
    #     if 'fav' not in request.session:
    #         request.session['fav'] = 0
    #     jml = request.session['fav']
    #     response['fav'] = jml
    #     if 'nama' not in request.session:
    #         request.session['nama'] = request.user.username
    #     if 'email' not in request.session:
    #         request.session['email'] = request.user.email
    # return render(request, 'books.html', response)
    return render(request, 'books.html', response)

def bookspage(request, cari='quilting'):
	link = 'https://www.googleapis.com/books/v1/volumes?q=' + cari
	daftar = requests.get(link).json()
	return JsonResponse(daftar)

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse(
        	{
        	'message':'Email format is wrong!',
            'status':'fail'
            })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse(
        	{
            'message':'Email already exist',
            'status':'fail'
        	})
    
    return JsonResponse(
    	{
        'message':'Email can be used',
        'status':'success'
    	})


def add_subscriber(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            'message':'Subscribe success!'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"There's no GET method here!"
        }, status = 403)

def web_service(request):
	data_user = Subscriber.objects.all().values('name', 'email')
	data = list(data_user)
	return JsonResponse(data, safe = False)

def delete_user(request):
	print('Login')
	print(request.POST['email'])
	select_user = Subscriber.objects.filter(email=request.POST['email'], password=request.POST['password'])

	if select_user:
		select_user.delete()
		return JsonResponse(
			{
			'message':request.POST['email'] + 'removed!',
			'deleted':True
			})

		return JsonResponse(
			{
			'message':'Incorrect password',
			'deleted':False
			})


'''def my_view(request):
	username = request.POST['username']
	password = request.POST['password']''
	user = authenticate(request, username=username, password=password)
	if user is not None:
		return JsonResponse({
            'message':'Login success!'
        }, status = 200)
	else:
		return JsonResponse({
            'message':"There's no GET method here!"
        }, status = 403)'''