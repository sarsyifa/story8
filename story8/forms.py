
from django import forms


class SubscribeForm(forms.Form):
    email = forms.EmailField(
        label = 'E-mail',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'email',
            'placeholder':'Enter Email',
            'type':'text',
            'id':'emailForm'
        })
    )
    name = forms.CharField(
        label = 'Name',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'name',
            'placeholder':'Enter Name',
            'type':'text',
            'id':'nameForm'
        })
    )
    password = forms.CharField(
        label = 'Password',
        required = True,
        max_length = 100,
        widget = forms.TextInput(attrs = {
            'name':'password',
            'placeholder':'Enter Password',
            'type':'password',
            'id':'passwordForm'
        })
    )