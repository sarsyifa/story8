"""appbang URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url 
from .views import index as landingpage
from .views import books, bookspage, check_email, add_subscriber, web_service, delete_user

urlpatterns = [
    path('', landingpage, name='landing-page'),
	path('books', books, name='books'),
    path('data', bookspage, name='bookspage'),
    path('check_email/', check_email, name = 'check_email'),
    path('add_subscriber/', add_subscriber, name = 'add_subscriber'),
    path('web_service/', web_service, name = 'web_service'),
    path('delete_user/', delete_user, name = 'delete_user')


]
